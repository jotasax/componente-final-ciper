/* eslint-disable jsx-a11y/no-onchange */
/* eslint-disable react/react-in-jsx-scope */
//import { useFetch } from "./hooks/useFetch";
import Loader from './Loader'
import Message from './Message'
import { useState } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { useQuery } from 'react-apollo'
import QUERY_BRANDS from './graphql/brands.gql'
import QUERY_CATEGORIES from './graphql/categories.gql'

const SelectList = ({ title, handless, url, handleChange }) => {
  let id = `select-${title}`
  let marcas = []
  let categorias = []
  let modelos = []
  let anios = []
  const { data } = useQuery(QUERY_BRANDS)
  const cat = useQuery(QUERY_CATEGORIES)

  let marcass
  let categoriess
  if (data && cat) {
    marcass = data.brands

    categoriess = cat.data.categories
  }

  for (let i = 2021; i >= 1960; i--) {
    anios.push(i)
  }

  if (title == 'categoría') {
    categorias = []
    if (data && cat) {
      for (let i = 1; i < categoriess.length; i++) {
        if (categoriess[i].hasChildren == true) {
          categorias.push(categoriess[i].name)
        }
      }
    }

    return (
      <select
        className={`${handless.buscadorcategoria}`}
        name={id}
        id={id}
        onChange={handleChange}
      >
        <option value="">{title.toUpperCase()}</option>
        {categorias.map(el => (
          <option key={el} value={el}>
            {el}
          </option>
        ))}
      </select>
    )
  } else if (title == 'marca') {
    marcas = []
    if (data) {
      for (let i = 1; i < marcass.length; i++) {
        if (marcass[i].active == true) {
          marcas.push(marcass[i].name)
        }
      }
    }

    return (
      <select
        className={`${handless.buscadormarca}`}
        name={id}
        id={id}
        onChange={handleChange}
      >
        <option value="">{title.toUpperCase()}</option>
        {marcas.map(el => (
          <option key={el} value={el}>
            {el}
          </option>
        ))}
      </select>
    )
  } else if (title == 'modelo') {
    switch (url) {
      case 'NISSAN':
        modelos = [
          'D21',
          'TERRANO-D22',
          'TIIDA',
          'MOTOR-LD28',
          'KICKS',
          'MURANO',
          'NAVARA',
          'MARCH',
          'QASHQAI',
          'JUKE',
          '620',
          'PULSAR',
          'SENTRA',
          'SUNNY',
          'V-16',
          'SENTRA-II',
          '1.8-BLUEBIRD',
          'SENTRA-2.0',
          'PRIMERA',
          'SENTRA-1.8',
          'PLATINA',
          'X-TRAIL',
          'PATHFINDER',
          'TERRANO-WD21',
          'V16-SW',
          'NP300',
          '150Y',
          '720',
          'VANETTE',
          'NV350',
          'TERRANO-II',
          'VERSA',
          'MISTRAL',
          '120Y',
          'SENTRA-II',
          '1.8-BLUEBIRD',
          'SENTRA-2.0',
          'PRIMERA',
          'LAUREL',
          'STANZA',
          'MOTOR-ED33',
          'MOTOR-RD28',
          'PATROL',
          '160J',
          '120A',
          '140Y',
          'SAMSUNG-SM5',
          'URBAN',
          'BLUEBIRD',
          'ALMERA',
          '130A',
          'TERRANO-D22X',
          'TODOS',
          'D22',
          '180B',
          'TEANA',
          'MOTOR-ED30',
          'MOTOR-TD23',
          'MOTOR-FD35',
          'MOTOR-CD20',
        ]
        modelos.sort()
        break

      case 'SUZUKI':
        modelos = [
          'IGNIS',
          'WAGON-R',
          'GRAN-VITARA',
          'APV',
          'CELERIO',
          'CARRY',
          'CERVO',
        ]
        modelos.sort()
        break

      case 'CHEVROLET':
        modelos = [
          'ASTRA',
          'LUV-DMAX',
          'CAVALIER',
          'ZAFIRA',
          'CAPTIVA',
          'LUV',
          'MONZA',
          'VIVANT',
          'AVEO',
          'N300',
          'COMBO',
          'SPARK',
          'APACHE',
          'WFR',
          'MONTANA',
          'CORSA',
          'TROOPER',
          'DMAX',
          'SERVFAIL',
          'COLORADO',
          'S10-AMERICANA',
        ]
        modelos.sort()
        break
      case 'MAHINDRA':
        modelos = [
          'SCORPIO',
          'PICK-UP',
          'GENIO',
          'PICK-UP-CRDE',
          'SCORPIO-CRDE',
          'XUV-500',
        ]
        modelos.sort()
        break
      case 'KIA':
        modelos = [
          'SORENTO-NEW',
          'RIO-5',
          'FRONTIER',
          'HI-BESTA',
          'PREGIO',
          'POP',
          'AVELLA',
          'RIO',
          'CERATO',
          'SORENTO',
          'K2400',
          'BESTA-ULTRA',
          'SPORTAGE',
          'MORNING',
          'K3600',
          'CARNIVAL',
          'K3500',
          'RIO-JB-LX',
          'CARENS',
          'BESTA-II',
          'SEPHIA',
          'GRAN-CARNIVAL',
          'BESTA',
          'PREGIO-ULTRA',
          'PRIDE',
          'SOUL',
          'CERES',
          'K4000',
          'TOPIC',
          'SPORTAGE-PRO',
          'CLARUS',
          'BESTA-GRAN',
          'OPTIMA',
          'NEW-SORENTO',
          'RIO-3',
          'FRONTIER-II',
          'GRAN-BESTA',
          'LOTZE',
          'KOUP',
        ]
        modelos.sort()
        break
      case 'TOYOTA':
        modelos = ['COROLLA', 'RAV-4', 'YARIS', 'AURIS', 'TODOS', 'HIACE']
        modelos.sort()
        break
      case 'ISUZU':
        modelos = ['NKR', 'NPR', 'NQR', 'FVR', 'FD1', 'FTR', 'GEMINI']
        modelos.sort()
        break
      case 'HYUNDAI':
        modelos = [
          'VERACRUZ',
          'MIGTHY',
          'H1',
          'ACCENT',
          'H100-PORTER',
          'PRIME',
          'SANTAMO',
          'NEW-ACCENT',
          'ACCENT-RB',
          'NEW-ELANTRA',
          'TUCSON',
          'STA-FE',
          'H100',
          'TERRACAN',
          'NEW-H1',
          'EXCEL',
          'MATRIX',
          'GETZ',
          'ELANTRA',
          'GALLOPER',
          'AVANTE',
          'I10',
          'SONATA-EF',
          'PORTER',
          'I30',
          'SONATA-NF',
          'COUNTY',
          'SONATA',
          'COUPE',
          'TRAJET',
          'ATOZ',
          'NEW-TUCSON',
          'SANTA-FE',
          'PORTER-HR',
          'NEW-PORTER',
          'CRETA',
          'NEW-SONATA',
          'EON',
          'NEW-STA-FE',
          'GENESIS',
          'GRAN-I10',
          'STELLAR',
          'SONATA-2.0-93/97',
          'VELOSTER',
        ]
        modelos.sort()
        break
      case 'SSANGYONG':
        modelos = [
          'ACTYON-SPORT',
          'ACTYON',
          'STAVIC',
          'ACTYON-SPORT-II',
          'KORANDO-C',
          'REXTON-2',
          'REXTON',
          'KORANDO',
          'MUSSO',
          'TIVOLI',
          'KYRON',
          'MUSO',
          'MUSSO-SPORT',
          'STAVIC-2',
        ]
        modelos.sort()
        break
      case 'DAIHATSU':
        modelos = [
          'CHARADE',
          'APPLAUSE',
          'FEROZA',
          'TERIOS',
          'MIRA',
          'GRAN-MOVE',
          'NEW-CUORE',
          'CUORE',
          'ROCKY',
          'FURGON',
        ]
        modelos.sort()
        break
      case 'MITSUBISHI':
        modelos = [
          'L200-DAKAR',
          'MONTERO-SPORT',
          'L100',
          'CANTER',
          'CHARIOT',
          'PAJERO',
          'L200',
          'GALANT',
          'L300',
          'ECLIPSE',
          'MONTERO',
          'MONTERO-SPORT',
          'L200',
          'L200-DAKAR',
          'LANCER',
          'GALANT',
          'L300',
          'DION',
          'COLT',
          'MONTERO',
          'OUTLANDER',
          'ROSA',
          'ECLIPSE',
          'CANTER',
          'PAJERO',
          'MIRAGE',
          'ASX',
          'L100',
          'L200-KATANA',
          'TODOS',
        ]
        modelos.sort()
        break
      case 'ASIA':
        modelos = ['TOPIC']
        modelos.sort()
        break
      case 'DAEWOO':
        modelos = ['NEXIA']
        modelos.sort()
        break
      case 'SUBARU':
        modelos = ['FURGON-600']
        modelos.sort()
        break
      case 'MAZDA':
        modelos = ['626', 'E2200']
        modelos.sort()
        break
      case 'FORD':
        modelos = ['CORCEL']
        modelos.sort()
        break
      default:
        break
    }
    return (
      <select
        className={`${handless.buscadormodelo}`}
        name={id}
        id={id}
        onChange={handleChange}
      >
        <option value="">{title.toUpperCase()}</option>
        {modelos.map(el => (
          <option key={el} value={el}>
            {el}
          </option>
        ))}
      </select>
    )
  } else if (title == 'año') {
    return (
      <select
        className={`${handless.buscadoranio}`}
        name={id}
        id={id}
        onChange={handleChange}
      >
        <option value="">{title.toUpperCase()}</option>
        {anios.map(el => (
          <option key={el} value={el}>
            {el}
          </option>
        ))}
      </select>
    )
  }
}

export default SelectList
