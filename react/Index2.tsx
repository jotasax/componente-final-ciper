/* eslint-disable jsx-a11y/no-onchange */
/* eslint-disable no-console */
import { injectIntl } from 'react-intl'
import { useCssHandles } from 'vtex.css-handles'
import { MyComponentProps } from './typings/global'
import { useState, useEffect } from 'react'
import React from 'react'

//Declare Handles for the react component to be accesible
const CSS_HANDLES = [
  'buscadorcustomizado',
  'buscadorcategoria',
  'buscadormarca',
  'buscadoranio',
  'buscadormodelo',
  'buscadorboton',
] as const

let activoMarca = true
let activoModelo = true
let activoAnio = true

const MyComponent: StorefrontFunctionComponent<MyComponentProps> = () => {
  const [categoria, setcategoria] = useState('')
  useEffect(() => {
    if (categoria != '' && categoria != 'cate') {
      activoMarca = false
    }
  })

  const [marca, setmarca] = useState('')
  useEffect(() => {
    if (marca != '' && marca != 'marca') {
      activoModelo = false
      switch (marca) {
        case 'NISSAN':
          break
        case '':
          console.log('It is a Monday.')
          break
        case '2':
          console.log('It is a Tuesday.')
          break
        case '3':
          console.log('It is a Wednesday.')
          break
        case '4':
          console.log('It is a Thursday.')
          break
        case '5':
          console.log('It is a Friday.')
          break
        case '6':
          console.log('It is a Saturday.')
          break
        default:
          console.log('No such day exists!')
          break
      }
    }
  })

  const [modelo, setmodelo] = useState('')
  useEffect(() => {
    if (modelo != '' && modelo != 'modelo') {
      activoAnio = false
    }
  })

  const [anio, setanio] = useState('')

  const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
    // Preventing the page from reloading
    event.preventDefault()

    // Do something
    let statusCategoria = false
    let statusAnio = false
    let statusMarca = false
    let statusModelo = false

    if (categoria != '') {
      statusCategoria = true
    }
    if (anio != '') {
      statusAnio = true
    }
    if (marca != '') {
      statusMarca = true
    }
    if (modelo != '') {
      statusModelo = true
    }

    //-------------------------validacion----------------------------------------//
    let url = ''

    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == false &&
      statusModelo == false
    ) {
      url =
        categoria +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == false &&
      statusModelo == false
    ) {
      url =
        categoria +
        '/' +
        anio +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,ano'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == true &&
      statusModelo == false
    ) {
      url =
        categoria +
        '/' +
        marca +
        '/' +
        anio +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand,ano'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == true &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        marca +
        '/' +
        anio +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand,ano,modelo'
    }
    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == true &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        marca +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand,modelo'
    }
    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == false &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,modelo'
    }
    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == true &&
      statusModelo == false
    ) {
      url =
        categoria +
        '/' +
        marca +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == false &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        anio +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,ano,modelo'
    }

    //url = "/"+categoria+"/"+marca+"/"+anio+"/"+modelo+"?initialMap=c&initialQuery="+categoria+"&map=category-1,brand,ano,modelo";
    console.log(url)
    let urlNueva = document.location.href + url
    document.location.href = urlNueva
  }

  const handles = useCssHandles(CSS_HANDLES)

  const anios = []

  anios.push(<option value="anio">Año</option>)
  for (let i = 1997; i <= 2021; i++) {
    const year = i
    anios.push(<option value={year}>{year}</option>)
  }

  const marcas = []
  marcas.push(<option value="NISSAN">NISSAN</option>)
  marcas.push(<option value="SUZUKI">SUZUKI</option>)
  marcas.push(<option value="CHEVROLET">CHEVROLET</option>)
  marcas.push(<option value="MAHINDRA">MAHINDRA</option>)
  marcas.push(<option value="KIA">KIA</option>)
  marcas.push(<option value="TOYOTA">TOYOTA</option>)
  marcas.push(<option value="ISUZU">ISUZU</option>)
  marcas.push(<option value="HYUNDAI">HYUNDAI</option>)
  marcas.push(<option value="SSANGYONG">SSANGYONG</option>)
  marcas.push(<option value="DAIHATSU">DAIHATSU</option>)
  marcas.push(<option value="MITSUBISHI">MITSUBISHI</option>)
  marcas.push(<option value="ASIA">ASIA</option>)
  marcas.push(<option value="DAEWOO">DAEWOO</option>)
  marcas.push(<option value="CHANGAN">CHANGAN</option>)
  marcas.push(<option value="SUBARU">SUBARU</option>)
  marcas.push(<option value="MAZDA">MAZDA</option>)
  marcas.push(<option value="HONDA">HONDA</option>)
  marcas.push(<option value="DATSUN">DATSUN</option>)
  marcas.push(<option value="FORD">FORD</option>)

  const categorias = []

  categorias.push(<option value="ACCESORIOS">ACCESORIOS</option>)
  categorias.push(<option value="BATERÍAS">BATERÍAS</option>)
  categorias.push(<option value="CARROCERÍA">CARROCERÍA</option>)
  categorias.push(<option value="DIRECCIÓN">DIRECCIÓN</option>)
  categorias.push(<option value="FRENOS">FRENOS</option>)
  categorias.push(<option value="LUBRICANTES">LUBRICANTES</option>)
  categorias.push(<option value="MOTOR">MOTOR</option>)
  categorias.push(<option value="RUEDAS">RUEDAS</option>)
  categorias.push(<option value="SISTEMA-ELÉCTRICO">SISTEMA ELÉCTRICO</option>)
  categorias.push(<option value="SUSPENSIÓN">SUSPENSIÓN</option>)
  categorias.push(<option value="TRANSMISIÓN">TRANSMISIÓN</option>)

  let modelos = []

  modelos.push(<option value="modelo">Modelo</option>)
  modelos.push(<option value="D21">D21</option>)
  modelos.push(<option value="TERRANO-D22">TERRANO-D22</option>)
  modelos.push(<option value="TIIDA">TIIDA</option>)
  modelos.push(<option value="NAVARA">NAVARA</option>)
  modelos.push(<option value="SUNNY">SUNNY</option>)
  modelos.push(<option value="SENTRA-2-0">SENTRA-2.0</option>)
  modelos.push(<option value="PRIMERA">PRIMERA</option>)
  modelos.push(<option value="V16-SW">V16-SW</option>)
  modelos.push(<option value="720">720</option>)

  return (
    <div>
      {/* Editable props on SiteEditor */}
      <div className={`${handles.buscadorcustomizado}`}>
        <form onSubmit={submitForm}>
          <select
            className={`${handles.buscadorcategoria}`}
            placeholder="categoria"
            value={categoria}
            onChange={e => setcategoria(e.target.value)}
          >
            {categorias}
          </select>
          <select
            disabled={activoMarca}
            className={`${handles.buscadormarca}`}
            placeholder="marca"
            value={marca}
            onChange={e => setmarca(e.target.value)}
          >
            {marcas}
          </select>
          <select
            disabled={activoModelo}
            className={`${handles.buscadormodelo}`}
            placeholder="modelo"
            value={modelo}
            onChange={e => setmodelo(e.target.value)}
          >
            {modelos}
          </select>
          <select
            disabled={activoAnio}
            className={`${handles.buscadoranio}`}
            placeholder="año"
            value={anio}
            onChange={e => setanio(e.target.value)}
          >
            {anios}
          </select>
          <button type="submit" className={`${handles.buscadorboton}`}>
            Buscar
          </button>
        </form>
      </div>
    </div>
  )
}

//This is the schema form that will render the editable props on SiteEditor
MyComponent.schema = {
  title: 'MyComponent Title',
  description: 'MyComponent description',
  type: 'object',
  properties: {
    someString: {
      title: 'SomeString Title',
      description: 'editor.my-component.someString.description',
      type: 'string',
      default: 'SomeString default value',
    },
  },
}

export default injectIntl(MyComponent)
