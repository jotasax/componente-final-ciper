import React, { Fragment, useState } from 'react'
import SelectList from './SelectList'
import { useCssHandles } from 'vtex.css-handles'
import { Slider } from 'vtex.styleguide'

const CSS_HANDLES = [
  'buscadorcustomizado',
  'buscadorcategoria',
  'buscadormarca',
  'buscadoranio',
  'buscadormodelo',
  'buscadorboton',
  'rangoAnio',
  'botonMenos',
  'botonMas',
  'botones',
]

const SelectsAnidados = () => {
  const [categoria, setCategoria] = useState('')
  const [marca, setMarca] = useState('')
  const [modelo, setModelo] = useState('')
  const [min, setMin] = useState(1993)
  const [max, setMax] = useState(2020)
  let annio = [2000, 2010]
  const [anio, setAnio] = useState([annio[0], annio[1]])

  const handles = useCssHandles(CSS_HANDLES)

  function handleSubmit(e) {
    e.preventDefault()

    // Do something
    let statusCategoria = false
    let statusAnio = false
    let statusMarca = false
    let statusModelo = false

    if (categoria != '') {
      statusCategoria = true
    }
    if (anio != '') {
      statusAnio = true
    }
    if (marca != '') {
      statusMarca = true
    }
    if (modelo != '') {
      statusModelo = true
    }

    //-------------------------validacion----------------------------------------//
    let url = ''
    let cadenaAnio = ''
    let aniosText = ''

    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == false &&
      statusModelo == false
    ) {
      url =
        categoria +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == false &&
      statusModelo == false
    ) {
      for (i = 0; i < anio.length; i++) {
        cadenaAnio = '/' + anio[i]
      }
      url =
        categoria +
        cadenaAnio +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,ano'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == true &&
      statusModelo == false
    ) {
      url =
        categoria +
        '/' +
        marca +
        '/' +
        anio +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand,ano'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == true &&
      statusModelo == true
    ) {
      let inicial = parseInt(anio[0])
      let final = parseInt(anio[anio.length - 1])
      for (let i = inicial; i < final; i++) {
        cadenaAnio = cadenaAnio + '/' + i
        aniosText = aniosText + ',ano'
      }
      url =
        categoria +
        '/' +
        marca +
        cadenaAnio +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand' +
        aniosText +
        ',modelo'
    }
    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == true &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        marca +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand,modelo'
    }
    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == false &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,modelo'
    }
    if (
      statusCategoria == true &&
      statusAnio == false &&
      statusMarca == true &&
      statusModelo == false
    ) {
      url =
        categoria +
        '/' +
        marca +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,brand'
    }
    if (
      statusCategoria == true &&
      statusAnio == true &&
      statusMarca == false &&
      statusModelo == true
    ) {
      url =
        categoria +
        '/' +
        anio +
        '/' +
        modelo +
        '?initialMap=c&initialQuery=' +
        categoria +
        '&map=category-1,ano,modelo'
    }

    let urlNueva = new URL(document.location.origin + '/' + url)

    window.location.href = urlNueva
  }

  function AddAnio() {
    setAnio([anio[0], anio[1] + 1])
    if (anio[1] > 2020) {
      setAnio([anio[0], anio[1]])
    }
  }

  function LessAnio() {
    setAnio([anio[0] - 1, anio[1]])
    if (anio[0] < 1994) {
      setAnio([anio[0], anio[1]])
    }
  }

  const [inputValues, setInputValues] = React.useState({
    left: min,
    right: max,
  })
  const [values, setValues] = React.useState({ left: min, right: max })

  const { left, right } = values

  const handleSubmitt = e => {
    e.preventDefault()
    console.log(e)
    console.log('anio', anio)
    const { left: leftValueInput, right: rightValueInput } = inputValues

    let left = Math.max(parseInt(leftValueInput) || 0, 0)
    let right = Math.min(parseInt(rightValueInput) || 99, 99)

    if (left > right) {
      return
    }

    setValues({
      left,
      right,
    })
  }

  return (
    <div className={`${handles.buscadorcustomizado}`}>
      <form onSubmit={handleSubmit}>
        <SelectList
          title="categoría"
          url=""
          handless={handles}
          handleChange={e => {
            setCategoria(e.target.value)
          }}
        />
        {!categoria && (
          <select className={`${handles.buscadormarca}`} disabled>
            <option value="0">MARCA</option>
          </select>
        )}
        {categoria && (
          <SelectList
            title="marca"
            url=""
            handless={handles}
            handleChange={e => {
              setMarca(e.target.value)
            }}
          />
        )}
        {!marca && (
          <select className={`${handles.buscadormodelo}`} disabled>
            <option value="0">MODELO</option>
          </select>
        )}
        {marca && (
          <SelectList
            title="modelo"
            url={marca}
            handless={handles}
            handleChange={e => {
              setModelo(e.target.value)
            }}
          />
        )}
        {/* {!anio && (
          <select className={`${handles.buscadoranio}`} disabled>
            <option value="0">AÑO</option>
          </select>
        )}
        {anio && (
          <select className={`${handles.buscadoranio}`}>
            <option value="0">AÑO</option>
          </select>
        )} */}
        <button type="submit" className={`${handles.buscadorboton}`}>
          Buscar
        </button>
        {modelo && (
          <div className={`${handles.rangoAnio}`} id="slider">
            <Slider
              onChange={e => {
                setAnio(e)
              }}
              min={min}
              max={max}
              step={1}
              disabled={false}
              defaultValues={[annio[0], annio[1]]}
              alwaysShowCurrentValue={false}
              values={[anio[0], anio[1]]}
              range
            />
            <div className={`${handles.botones}`}>
              <input
                className={`${handles.botonMenos}`}
                type="button"
                value="-"
                onClick={e => LessAnio()}
              ></input>
              <input
                className={`${handles.botonMas}`}
                type="button"
                value="+"
                onClick={e => AddAnio()}
              ></input>
            </div>
          </div>
        )}
      </form>
    </div>
  )
}

export default SelectsAnidados
